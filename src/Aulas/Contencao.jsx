import React from 'react'
import Parents from './ContencaoII'

/* Contenção - caputrar elementos que estão dentro de 
    outros elementos, usando props.
        Esses outros elementos são
            os props.children.   
*/

const pai= 'Juvenildo F. Lima'
const filho = 'Diego J. Lima'

export default () =>{
    return (
        <div>
            <h1>Conteção</h1>,
            <Parents>
                <h2>Olá sou o pai, {pai}</h2>
                <h3>Olá sou o filho, {filho}</h3>
            </Parents>
        </div>
    )
}