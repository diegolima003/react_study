import React, {useEffect, useState} from 'react'
 
// Usaremos o segundo hook - UseEfect     (O primeiro hook foi o useState)
// Useeffect é um hook que será chamada assim que a pagina for atualizada

export default () => {
    
    const [contagem, setContagem]=useState(0)
    useEffect(
        ()=> {
            console.log('Recarregou '+contagem)
            document.title='Contagem '+contagem
        }
    )
    
    return(
        <>
            <h1>Hook - Use Effect</h1>
            
            <p>Contando: {contagem}</p>
            <button onClick={()=>setContagem(contagem+1)}>Botão</button>
        </>
    )
}